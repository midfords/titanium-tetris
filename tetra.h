
#ifndef TETRA_H_
#define TETRA_H_

#define TETRA_COUNT 7

typedef struct tetra {
  int l;
  int shape[16];
  int (*no_func) (int, int, int);
  int (*we_func) (int, int, int);
  int (*so_func) (int, int, int);
  int (*ea_func) (int, int, int);
} tetra;

extern const tetra test;
extern const tetra t0;
extern const tetra t1;
extern const tetra t2;
extern const tetra t3;
extern const tetra t4;
extern const tetra t5;
extern const tetra t6;

void init_tetra(int n);

void freeTetra();

tetra requestTetra(int i);

#endif
