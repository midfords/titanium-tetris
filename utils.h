
#ifndef UTILS_H_
#define UTILS_H_

#include "game.h"

int p(int y, int x, int h, int w);

int no(int y, int x, int l);

int we(int y, int x, int l);

int so(int y, int x, int l);

int ea(int y, int x, int l);

void processTetraShadow(int* brd, tetra_state* s, tetra_state* shadow);

void writeTetraShadowToBoard(int* brd, tetra_state* s);

void writeTetraToBoard(int* brd, tetra_state* ts);

#endif
