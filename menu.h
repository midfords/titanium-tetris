
#ifndef MENU_H_
#define MENU_H_

#include <curses.h>
#include <pthread.h>
#include "print.h"

window* initMenuWin();

window* initBoarderWin();

void printMenu(window* w);

void printMenuImage(window* w);

void printBoarder(window* w);

pthread_t startMenuAnimateThread(window* w);

#endif
