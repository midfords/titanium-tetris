#include "random.h"

#include <limits.h>

double getDouble(unsigned long *seed) {
  unsigned long s = (unsigned long)*seed * 48271;
  unsigned int x = (s & 0x7fffffff) + (s >> 31);
  x = (x & 0x7fffffff) + (x >> 31);
  *seed = x;
  return (double)x / (double)INT_MAX;
}
