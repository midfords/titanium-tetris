
#ifndef INPUT_H_
#define INPUT_H_

#include <pthread.h>

#include "tetris.h"
#include "signaler.h"

#define KEY_ESC 27

#define ACTION_UP 1
#define ACTION_DOWN 2
#define ACTION_LEFT 3
#define ACTION_RIGHT 4
#define ACTION_RLEFT 5
#define ACTION_RRIGHT 6
#define ACTION_PAUSE 7
#define ACTION_QUIT 8

typedef struct keyboard_args {
  signaler* player_si[MAX_PLAYERS];
  signaler* game_si;
} kbargs;

int get_raw_input();

void* keyboard_t(void* arg);

#endif
