# Tetris
Tetris written for *Unix* based systems and *MacOS*.

This is my take on the classic game Tetris. Written in C for Unix and MacOS. The game includes one to four player modes, game animations, and some pretty sweet menu art. Total time clocked writing this game was approximately 80 hours (12 of those were for colourizing and animating the menu art alone ಠ_ಠ ).

Anyway I’m pretty proud of this one, I pulled out all the tricks while programming this. Whoever you are, you’re probably the first person to find and read this -- but I hope you enjoy the game!

## How to play
The object of the game is to reach a high score by stacking and arranging blocks into complete rows. Complete rows clear and award additional points.

Points are awarded as follows:
- Tetra placed: *50* pts
- Single: *200* pts
- Double: *500* pts
- Triple: *1000* pts
- Tetris: *2000* pts

Level and speed increase every 7500 points.

## Controls
From the main menu:
- <kbd>1</kbd> or <kbd>Space</kbd> One player game
- <kbd>2</kbd> Two player game
- <kbd>3</kbd> Three player game
- <kbd>4</kbd> Four player game
- <kbd>Q</kbd> Exit program

You can always exit the program with <kbd>Control</kbd>+<kbd>C</kbd>.

Pause and unpause the game with <kbd>P</kbd>.
Quit by pressing <kbd>Q</kbd> from the pause menu.

Player 1 game controls:
- <kbd>↑</kbd> Set piece down
- <kbd>↓</kbd> Move piece down
- <kbd>←</kbd> Move piece left
- <kbd>→</kbd> Move piece right
- <kbd>Space</kbd> Rotate piece clockwise
- <kbd>.</kbd> Rotate piece counter clockwise

Player 2 game controls:
- <kbd>W</kbd> Set piece down
- <kbd>S</kbd> Move piece down
- <kbd>A</kbd> Move piece left
- <kbd>D</kbd> Move piece right
- <kbd>Z</kbd> Rotate piece clockwise
- <kbd>X</kbd> Rotate piece counter clockwise

Player 3 game controls:
- <kbd>T</kbd> Set piece down
- <kbd>F</kbd> Move piece down
- <kbd>G</kbd> Move piece left
- <kbd>H</kbd> Move piece right
- <kbd>V</kbd> Rotate piece clockwise
- <kbd>B</kbd> Rotate piece counter clockwise

Player 4 game controls:
- <kbd>I</kbd> Set piece down
- <kbd>J</kbd> Move piece down
- <kbd>K</kbd> Move piece left
- <kbd>L</kbd> Move piece right
- <kbd>M</kbd> Rotate piece clockwise
- <kbd>,</kbd> Rotate piece counter clockwise

## Installation and Setup
- This program is written for *Unix* and *MacOS*, if you run Windows you will need to install a linux distro first with Linux Subsystem for Windows.
- You may need to install common command line tools like **git** and **gcc** first.

Clone the repository using git

`git clone https://github.com/midfords/tetris.git tetris-midfords && cd tetris-midfords`

You may need to set permissions on the source code before compiling.

`chown 777 *`

Compile the project by simply running make

`make`

Run the game with

`./tetris`

## Screenshots
![Alt](/images/screenshot-menu.png "Screenshot")
![Alt](/images/screenshot-1player.png "Screenshot")
![Alt](/images/screenshot-4player.png "Screenshot")

