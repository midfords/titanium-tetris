
#include "menu.h"

#include <stdlib.h>

#include "signaler.h"
#include "utils.h"

window wm = {
  -1, NULL, NULL
};

window wb = {
  -1, NULL, NULL
};

window* initMenuWin() {
  wm.win = newwin(28, 49, 3, 6);
  return &wm;
}

window* initBoarderWin() {
  wb.win = newwin(35, 62, 0, 0);
  return &wb;
}

void printBoarder(window* w) {
  mvwprintw(w->win, 0, 0,  " ___________________________________________________________ ");
  mvwprintw(w->win, 1, 0,  "|_   _|_  |_   _|______|  _| |  _|______|   | |___  |  ___| |");
  mvwprintw(w->win, 2, 0,  "| |_|___|___|_|______|___|___|_|____|___|___|_____|_|_|_  | |");
  mvwprintw(w->win, 3, 0,  "|___|                                                   | | |");
  mvwprintw(w->win, 4, 0,  "|  _|                                                   |_|_|");
  mvwprintw(w->win, 5, 0,  "| | |                                                   |   |");
  mvwprintw(w->win, 6, 0,  "|_| |                                                   |___|");
  mvwprintw(w->win, 7, 0,  "| | |                                                   |  _|");
  mvwprintw(w->win, 8, 0,  "| |_|                                                   | | |");
  mvwprintw(w->win, 9, 0,  "|_| |                                                   |_| |");
  mvwprintw(w->win, 10, 0, "|_  |                                                   | | |");
  mvwprintw(w->win, 11, 0, "| |_|                                                   |_|_|");
  mvwprintw(w->win, 12, 0, "|_  |                                                   |  _|");
  mvwprintw(w->win, 13, 0, "| |_|                                                   |_| |");
  mvwprintw(w->win, 14, 0, "|___|                                                   |  _|");
  mvwprintw(w->win, 15, 0, "|   |                                                   |_| |");
  mvwprintw(w->win, 16, 0, "|___|                                                   | | |");
  mvwprintw(w->win, 17, 0, "|_  |                                                   |_| |");
  mvwprintw(w->win, 18, 0, "| | |                                                   | |_|");
  mvwprintw(w->win, 19, 0, "| |_|                                                   |  _|");
  mvwprintw(w->win, 20, 0, "|___|                                                   |_| |");
  mvwprintw(w->win, 21, 0, "|  _|                                                   |_  |");
  mvwprintw(w->win, 22, 0, "| |_|                                                   | |_|");
  mvwprintw(w->win, 23, 0, "|_| |                                                   | | |");
  mvwprintw(w->win, 24, 0, "|_  |                                                   |_|_|");
  mvwprintw(w->win, 25, 0, "| |_|                                                   |   |");
  mvwprintw(w->win, 26, 0, "|_  |                                                   |___|");
  mvwprintw(w->win, 27, 0, "| |_|                                                   |  _|");
  mvwprintw(w->win, 28, 0, "|___|                                                   | | |");
  mvwprintw(w->win, 29, 0, "|  _|                                                   |_| |");
  mvwprintw(w->win, 30, 0, "| | |                                                   | | |");
  mvwprintw(w->win, 31, 0, "|_| |___________________________________________________| |_|");
  mvwprintw(w->win, 32, 0, "| |___|   |_| |______| |___  |_|  _| |______| |_______|___| |");
  mvwprintw(w->win, 33, 0, "|_____|___|_____|____|_____|_|___|_____|__|_____|__|______|_|");

  wmove(w->win, 34, 61);
  wrefresh(w->win);
}

void printMenuOptions(window* w) {
  mvwprintw(w->win, 1,  3, "+--------------+");
  mvwprintw(w->win, 2,  3, "|              |");
  mvwprintw(w->win, 3,  3, "|  [1] Player  |");
  mvwprintw(w->win, 4,  3, "|  [2] Player  |");
  mvwprintw(w->win, 5,  3, "|  [3] Player  |");
  mvwprintw(w->win, 6,  3, "|  [4] Player  |");
  mvwprintw(w->win, 7,  3, "|              |");
  mvwprintw(w->win, 8,  3, "|    [Q]uit    |");
  mvwprintw(w->win, 9,  3, "|              |");
  mvwprintw(w->win, 10,  3, "|              |");
  mvwprintw(w->win, 11,  3, "| v1.0    2019 |");
  mvwprintw(w->win, 12,  3, "+--------------+");
}

void printTitle(window* w) {
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24,  0, "  _____ ___ _____ ___  _  __  ");
  mvwprintw(w->win, 25,  0, " |_   _| __|_   _| _ \\| |/ _| ");
  mvwprintw(w->win, 26,  0, "   | | | _|  | | |   /| |\\ \\  ");
  mvwprintw(w->win, 27,  0, "   |_| |___| |_| |_|_\\|_|__/  ");
  wattroff(w->win, COLOR_PAIR(13));
}

void printMenuImage(window* w) {

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 0, 0, "                           ,");
  mvwprintw(w->win, 1, 0, "                           T");
  mvwprintw(w->win, 2, 0, "                          ( )");

  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 3, 0, "                          <");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 3, 27, "==");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 3, 29, ">");

  mvwprintw(w->win, 4, 0, "                           FJ");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));

  mvwprintw(w->win, 5, 0, "                           ==");

  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 6, 0, "                          J");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 6, 27, "||");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 6, 29, "F");

  mvwprintw(w->win, 7, 0, "                          F");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 7, 27, "||");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 7, 29, "J");

  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 8, 0, "                         /\\/\\/\\");

  mvwprintw(w->win, 9, 0, "                         F");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 9, 26, "++++");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 9, 30, "J");

  mvwprintw(w->win, 10, 0, "                        J{");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 10, 26, "}");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 10, 27, "{}");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 10, 29, "{");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 10, 30, "}F");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 10, 41, ".");

  mvwprintw(w->win, 11, 0, "                     .");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 11, 22, "  F");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 11, 25, "{");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 11, 26, "}{}{");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 11, 30, "}");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 11, 31, "J");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 11, 32, "         T");

  mvwprintw(w->win, 12, 0, "          .          T");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 12, 22, " J");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 12, 24, "{}");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 12, 26, "{}{}");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 12, 30, "{}");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 12, 32, "F");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 12, 33, "        ;");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 12, 42, ";");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 13, 0, "          T");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 13, 11, "         /|\\");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 13, 23, "F");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 13, 24, " \\/ \\/ \\");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 13, 32, "J");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 13, 33, "  .");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 13, 36, "   ,");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 13, 40, ";");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 13, 41, ";");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 13, 42, ";");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 13, 43, ";");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 13, 44, ".");

  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 14, 0, "         /");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 14, 10, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 14, 11, "\\");
  wattroff(w->win, COLOR_PAIR(12));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 14, 12, "      .");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 14, 19, "'");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 14, 20, "/|\\");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 14, 23, "\\");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 14, 24, ":");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 14, 25, "========");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 14, 33, "F");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 14, 34, " T .");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 14, 38, "/");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 14, 39, ";");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 14, 40, ";");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 14, 41, ";");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 14, 42, ";");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 14, 43, ";");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 14, 44, ";");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 14, 45, "\\");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 15, 0, "       .");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 15, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 15, 9, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 15, 10, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 15, 11, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 15, 12, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 15, 13, ".");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 15, 14, "   /");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 15, 18, "/");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 15, 19, "/");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 15, 20, "|");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 15, 21, "|");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 15, 22, "|");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 15, 23, "\\");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 15, 24, "\\");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 15, 25, "\\\"\"\"\"\"\"\" ");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 15, 34, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 15, 35, "x");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 15, 36, "\\");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 15, 37, "T");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 15, 38, "\\");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 15, 39, ";");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 15, 40, ";");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 15, 41, ";");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 15, 42, ";");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 15, 43, ";");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 15, 44, ";");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 15, 45, "/");

  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 16, 0, "      //");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 16, 8, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 16, 9, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 16, 10, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 16, 11, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 16, 12, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 16, 13, "/\\");
  wattroff(w->win, COLOR_PAIR(12));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 16, 15, "  \\");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 16, 18, "\\");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 16, 19, "\\");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 16, 20, "\\");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 16, 21, "|");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 16, 22, "/");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 16, 23, "/");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 16, 24, "/");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 16, 25, "/");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 16, 26, ".");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 16, 27, ".");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 16, 28, "[]");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 16, 30, ".");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 16, 31, ".");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 16, 32, ".");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 16, 33, "x");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 16, 34, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 16, 35, "X");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 16, 36, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 16, 37, "x.");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 16, 39, "|====|");

  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 17, 0, "      \\");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 17, 7, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 17, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 17, 9, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 17, 10, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 17, 11, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 17, 12, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 17, 13, ":T7");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 17, 16, " :.:.:.:.:");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 17, 26, "|");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 17, 27, "|");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 17, 28, "[]");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 17, 30, "|");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 17, 31, "/x");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 17, 33, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 17, 34, "X");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 17, 35, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 17, 36, "X");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 17, 37, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 17, 38, "x\\");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 17, 40, "|");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 17, 41, "|");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 17, 42, "||");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 17, 44, "|");

  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 18, 0, "       :");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 18, 8, ".");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 18, 9, ":");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 18, 10, ".");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 18, 11, ":");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 18, 12, ".");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 18, 13, ":");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 18, 14, "A.");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 18, 16, " `;:;:;:;'");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 18, 26, "=====");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 18, 31, "\\");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 18, 32, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 18, 33, "X");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 18, 34, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 18, 35, "X");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 18, 36, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 18, 37, "X");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 18, 38, "X");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 18, 39, "/");
  wattroff(w->win, COLOR_PAIR(12));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 18, 40, "=");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 18, 41, "=");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 18, 42, "=");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 18, 43, "=");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 18, 44, "=");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 18, 45, ".");

  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 0, "      `");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 19, 7, ";");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 8, "\"");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 19, 9, "\"");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 10, "::");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 19, 12, "/x");
  wattron(w->win, COLOR_PAIR(15));
  mvwprintw(w->win, 19, 14, "x");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 19, 15, "x\\");
  wattroff(w->win, COLOR_PAIR(14));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 17, ".");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 19, 18, "|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 19, 19, ",");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 19, 20, "|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 19, 21, ",");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 19, 22, "|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 19, 23, ",");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 19, 24, "|");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 25, " ( )( )");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 19, 32, "|");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 33, " | |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 19, 37, " |.");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 40, "=");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 19, 41, "..");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 43, "=");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 19, 44, ".");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 19, 45, "|");

  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 0, "       :");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 20, 8, ".");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 9, " :");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 20, 11, "`");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 20, 12, "\\");
  wattron(w->win, COLOR_PAIR(15));
  mvwprintw(w->win, 20, 13, "x");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 20, 14, "x");
  wattron(w->win, COLOR_PAIR(15));
  mvwprintw(w->win, 20, 15, "x");
  wattron(w->win, COLOR_PAIR(14));
  mvwprintw(w->win, 20, 16, "/");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 17, "(");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 20, 18, "_");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 19, ")(");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 20, 21, "_");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 22, ")(");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 20, 24, "_");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 25, ")");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 20, 26, " _  _");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 31, " | | | |'");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 20, 40, "-");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 41, "''");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 20, 43, "-");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 20, 44, "'");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 20, 45, "|");

  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 21, 0, "       :");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 21, 8, "T");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 21, 9, "-");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 21, 10, "'");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 21, 11, "-.");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 21, 13, ":");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 21, 14, "\"\"");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 21, 16, ":|");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 21, 18, "\"\"\"\"\"\"\"");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 21, 25, "|");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 21, 26, "/ \\/ \\");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 21, 32, "|=====|");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 21, 39, "======");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 21, 45, "|");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 22, 0, "       .");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 22, 8, "A");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 22, 9, ".");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 22, 10, "\"\"\"||_|| ,");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 22, 20, ". .");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 22, 23, ". |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 22, 26, "|");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 22, 27, " ||");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 22, 30, " |");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 22, 32, "/\\/\\/\\/");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 22, 39, " |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 22, 41, " |");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 22, 43, " |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 22, 45, "|");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 0, "   :");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 4, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 5, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 6, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 7, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 10, "\\");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 11, ":::.");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 15, "'.| |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 20, "| |");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 23, "| ||");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 27, "-");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 28, "||");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 30, "-");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 31, "|");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 32, "/\\/\\/\\");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 38, "+|+");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 41, "|");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 42, " |");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 44, " |");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 0, "  ;:;;");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 6, "\\");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 7, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 10, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 11, "::::");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 24, 15, ",='");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 24, 18, "=======");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 24, 25, "'");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 24, 26, "============");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 38, "/\\/\\");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 24, 42, "=====");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 24, 47, ".");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 0, ":");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 1, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 2, ":::;");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 6, "\"\"\"");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 9, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 14, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 15, ":");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 16, "|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 17, "__");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 19, "..,");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 22, "__");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 24, "|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 25, "===========");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 25, 36, "/||\\|\\");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 42, "====");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 46, "|");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 0, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 5, ";");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 26, 6, "|");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 7, "=");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 8, ":::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 11, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 12, ":");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 13, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 14, "::");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 26, 16, "|");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 17, ",");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 18, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 19, ":::::");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 26, 24, "         |");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 26, 34, "========");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 26, 42, "|   |");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 0, "::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 2, ";;;");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 5, "::::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 11, "(");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 27, 12, "}");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 13, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 18, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 19, "::::::");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 27, 25, "________|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 27, 34, "========");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 27, 42, "|___|_");

  wmove(w->win, 27, 48);
  wrefresh(w->win);
}

void printMenu(window* w) {
  printMenuImage(w);
  printMenuOptions(w);
  printTitle(w);

  wmove(w->win, 27, 48);
  wrefresh(w->win);
}

void printMenuFrame1(window* w) {

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 0, "   :");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 4, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 5, ":");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 6, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 7, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 10, "\\");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 11, ":::.");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 15, "'.| |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 20, "| |");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 23, "| ");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 0, "  ;:;;");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 6, "\\");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 7, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 10, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 11, "::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 24, 15, ",");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 24, 16, "='");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 24, 18, "=======");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 0, ":");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 1, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 2, ":::;");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 6, "\"\"\"");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 9, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 14, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 15, ":");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 16, "|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 17, "__");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 19, "..,");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 22, "__");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 24, "|");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 0, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 5, ";");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 26, 6, "|");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 7, "=");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 8, ":::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 11, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 12, ":");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 13, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 14, "::");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 26, 16, "|");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 17, ",");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 18, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 19, "::::: ");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 0, "::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 2, ";;;");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 5, "::::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 11, "(");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 27, 12, "}");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 13, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 18, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 19, "::::::");


  wmove(w->win, 27, 48);
  wrefresh(w->win);
}

void printMenuFrame2(window* w) {

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 0, "    ::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 6, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 7, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 10, "\\");
  wattroff(w->win, COLOR_PAIR(12));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 11, "=");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 12, ".::");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 15, "'.| |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 20, "| |");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 23, "| ");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 0, "   :;;:.");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 10, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 11, "::::;");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 24, 16, "='");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 24, 18, "=======");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 0, " :");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 2, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 3, "::;:");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 7, "\"\"");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 9, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 14, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 15, ":");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 16, "|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 17, "___");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 20, ".,");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 22, "__");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 24, "|");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 0, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 5, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 6, ":");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 7, "==");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 9, "::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 11, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 12, "::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 14, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 15, ":");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 26, 16, "|");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 17, ".:");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 19, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 20, ":;:. ");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 0, "::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 2, ";;;");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 5, "::::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 11, "(");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 27, 12, "}");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 13, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 18, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 19, ":::::.");


  wmove(w->win, 27, 48);
  wrefresh(w->win);
}

void printMenuFrame3(window* w) {

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 0, "    .:");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 6, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 7, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 10, "\\");
  wattroff(w->win, COLOR_PAIR(12));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 11, "==");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 13, "::");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 15, "'.| |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 20, "| |");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 23, "| ");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 0, "   .:;:,");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 10, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 11, ".::::,");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 24, 17, "'");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 24, 18, "=======");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 0, " .:");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 3, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 4, ":;:");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 7, "\"\"\"");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 10, ".:::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 14, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 15, ":");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 16, "|");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 17, "___");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 20, "..");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 22, "__");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 24, "|");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 0, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 5, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 6, ":.");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 8, "=");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 9, ".::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 14, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 15, ":; .:");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 20, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 21, "::, ");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 0, ":::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 3, ";;");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 5, ":::::::");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 27, 12, "}");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 13, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 18, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 19, ":::::.");


  wmove(w->win, 27, 48);
  wrefresh(w->win);
}

void printMenuFrame4(window* w) {

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 0, "     .");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 6, ",");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 7, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 23, 10, "\\");
  wattroff(w->win, COLOR_PAIR(12));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 11, "==");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 23, 13, ".:");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 15, "'.| |");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 23, 20, "| |");
  wattroff(w->win, COLOR_PAIR(11));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 23, 23, "| ");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 0, "    :;:;");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 8, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 9, "/");
  wattron(w->win, COLOR_PAIR(12));
  mvwprintw(w->win, 24, 10, "/");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 24, 11, " .:::,");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 24, 17, "'");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 24, 18, "=======");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 0, " .:");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 3, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 4, ":;:.");
  wattroff(w->win, COLOR_PAIR(13));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 8, "\"\"");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 10, ".:::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 25, 14, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 15, "::");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 17, "____");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 25, 21, ".");
  wattron(w->win, COLOR_PAIR(16));
  mvwprintw(w->win, 25, 22, "__");
  wattroff(w->win, COLOR_PAIR(16));
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, 25, 24, "|");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 0, ":::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 5, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 6, ":.");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 8, "=");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 9, ".::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 14, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 15, "::. .");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 26, 20, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 26, 21, "::; ");

  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 0, ":::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 3, ";;");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 5, ":::::::.::::::");
  wattron(w->win, COLOR_PAIR(11));
  mvwprintw(w->win, 27, 19, ";");
  wattron(w->win, COLOR_PAIR(13));
  mvwprintw(w->win, 27, 20, "::::,");


  wmove(w->win, 27, 48);
  wrefresh(w->win);
}

void runAnimation1(window* w) {
    printMenuFrame1(w);
    msleep(NULL, 150);

    printMenuFrame2(w);
    msleep(NULL, 500);

    printMenuFrame1(w);
    msleep(NULL, 150);
}

void runAnimation2(window* w) {
    printMenuFrame1(w);
    msleep(NULL, 300);

    printMenuFrame2(w);
    msleep(NULL, 300);

    printMenuFrame3(w);
    msleep(NULL, 500);

    printMenuFrame2(w);
    msleep(NULL, 300);

    printMenuFrame1(w);
    msleep(NULL, 300);
}

void runAnimation3(window* w) {
    printMenuFrame1(w);
    msleep(NULL, 200);

    printMenuFrame2(w);
    msleep(NULL, 200);

    printMenuFrame3(w);
    msleep(NULL, 200);

    printMenuFrame4(w);
    msleep(NULL, 500);

    printMenuFrame3(w);
    msleep(NULL, 300);

    printMenuFrame2(w);
    msleep(NULL, 200);

    printMenuFrame3(w);
    msleep(NULL, 200);

    printMenuFrame4(w);
    msleep(NULL, 500);

    printMenuFrame3(w);
    msleep(NULL, 300);

    printMenuFrame2(w);
    msleep(NULL, 200);

    printMenuFrame1(w);
    msleep(NULL, 200);
}

void runAnimation4(window* w) {
    printMenuFrame1(w);
    msleep(NULL, 100);

    printMenuFrame2(w);
    msleep(NULL, 100);

    printMenuFrame3(w);
    msleep(NULL, 100);

    printMenuFrame4(w);
    msleep(NULL, 600);

    printMenuFrame3(w);
    msleep(NULL, 200);

    printMenuFrame2(w);
    msleep(NULL, 200);

    printMenuFrame1(w);
    msleep(NULL, 200);
}

typedef struct menu_args {
  window* win;
} menu_args;

void* animMenuThread(void* arg) {
  menu_args* args = (menu_args*) arg;
  window* w = args->win;
  free(args);

  msleep(NULL, 400);

  while(1) {
    msleep(NULL, 400);
    int an = rand() % 4;
    switch(an) {
      case 0:
        runAnimation1(w);
        break;
      case 1:
        runAnimation2(w);
        break;
      case 2:
        runAnimation3(w);
        break;
      case 3:
        runAnimation4(w);
        break;
    }
  }

  pthread_exit(NULL);
}

void initRng() {
  srand((unsigned) time(NULL));
}

pthread_t startMenuAnimateThread(window* w) {
  pthread_t anim_t;
  menu_args* args = (menu_args*) malloc(sizeof(menu_args));
  args->win = w;

  pthread_create(&anim_t, NULL, animMenuThread, (void*) args);

  return anim_t;
}
