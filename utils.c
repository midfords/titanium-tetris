
#include "utils.h"

#include <stdlib.h>
#include <time.h>

#include "game.h"

int p(int y, int x, int h, int w) { return x + (w * y); }

int no(int y, int x, int l) { return x + (l * y); }

int we(int y, int x, int l) { return l * (x + 1) - 1 - y; }

int so(int y, int x, int l) { return (l * l - 1) - (x + (l * y)); }

int ea(int y, int x, int l) { return l * (l - x - 1) + y; }

void processTetraShadow(int* brd, tetra_state* s, tetra_state* shadow) {
  shadow->dir_ptr = s->dir_ptr;
  shadow->t = s->t;
  shadow->x = s->x;
  shadow->y = s->y;

  setDown(brd, shadow);
}

void writeTetraShadowToBoard(int* brd, tetra_state* s) {
  tetra t = s->t;

  int i, j;
  for (i = 0; i < t.l; i++) {
    for (j = 0; j < t.l; j++) {
      if (t.shape[s->dir_ptr(i, j, t.l)] > 0) {
        brd[p(i + s->y, j + s->x, BOARD_Y, BOARD_X)] = -1;
      }
    }
  }
}

void writeTetraToBoard(int* brd, tetra_state* s) {
  tetra t = s->t;

  int i, j;
  for (i = 0; i < t.l; i++) {
    for (j = 0; j < t.l; j++) {
      if (t.shape[s->dir_ptr(i, j, t.l)] > 0) {
        brd[p(i + s->y, j + s->x, BOARD_Y, BOARD_X)] =
            t.shape[s->dir_ptr(i, j, t.l)];
      }
    }
  }
}
