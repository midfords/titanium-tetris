#include "tetris.h"

#include <ctype.h>
#include <curses.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "game.h"
#include "input.h"
#include "menu.h"
#include "print.h"
#include "signaler.h"
#include "tetra.h"
#include "utils.h"

tetra_state* initNewTetra(int p) {
  tetra_state* s = (tetra_state*) malloc(sizeof(tetra_state));
  s->t = requestTetra(p);
  s->dir_ptr = no;
  s->y = 0;
  s->x = BOARD_X / 2 - 2;

  return s;
}

int isGameOver( int* brd, tetra_state* s ) {
  tetra t = s->t;
  int over = 0;

  int i, j;
  for ( i = 0; i < t.l; i++ ) {
    for ( j = 0; j < t.l; j++ ) {
      int b = t.shape[s->dir_ptr(i, j, t.l)];
      int ry = s->y + i;
      int rx = s->x + j;

      over = over || ( ( b != 0 ) && 
        ( brd[p(ry, rx, BOARD_Y, BOARD_X)] != 0 ) );
    }
  }

  return over;
}

int clearLines(window* w, int* brd) {
  pthread_t anim_t[4];
  int lines[4];
  int n = 0;

  int i, j;
  for (i = BOARD_Y - 1; i >= 0; i--) {
    int clear = 1;
    for (j = 0; j < BOARD_X; j++) {
      if (i - n >= 0)
        brd[p(i, j, BOARD_Y, BOARD_X)] = brd[p(i - n, j, BOARD_Y, BOARD_X)];
      else
        brd[p(i, j, BOARD_Y, BOARD_X)] = 0;

      clear = clear && ( brd[p(i, j, BOARD_Y, BOARD_X)] > 0 );
    }

    if (clear) {
      lines[n] = i - n;
      n++;
      i++;
    }
  }

  for (i = 0; i < n; i++) {
    anim_t[i] = start_clear_line_animation(w, lines[i], n >= 4);
  }

  for (i = 0; i < n; i++) {
    pthread_join(anim_t[i], NULL);
  }

  if (n > 0)
    redraw_board(w, brd);

  return n;
}

void setCurrentPiece(window* w, game_state* g, signaler* in, signaler* co) {
  writeTetraToBoard(g->brd, g->curr);
  free(g->curr);
  g->curr = g->next;
  g->next = initNewTetra(g->player);
  
  update_preview(w, g->next);

  int li = clearLines(w, g->brd);
  int sc = calcScore(li);
  g->lines += li;
  g->score += sc;

  int le = calcLevel(g->score);
  g->level = le;

  update_summary(w, g->time, g->level, g->score);
  update_board(w, g->brd, g->curr);

  g->gameover = isGameOver(g->brd, g->curr);
  send_signal(in);
  send_signal(co);
}

void incrementTimer(game_state* g) {
  pthread_mutex_lock(&g->mutex);
  g->time++;
  pthread_mutex_unlock(&g->mutex);
}

void sendScoreboardUpdate(window* w, game_state* g) {
  pthread_mutex_lock(&g->mutex);
  update_summary(w, g->time, g->level, g->score);
  pthread_mutex_unlock(&g->mutex);
}

typedef struct player_args {
  pthread_mutex_t suspend_time;
  pthread_mutex_t suspend_fall;
  pthread_mutex_t suspend_game;
  window* win;
  signaler* control;
  signaler* queue;
  signaler* timer;
  signaler* fall;
  game_state* game_state;
} player_args;

void *timer_t (void* arg) {
  player_args* a = (player_args*) arg;
  window* w = a->win;
  signaler* s = a->timer;
  game_state* g = a->game_state;

  while (!is_signaled(s)) {
    incrementTimer(g);
    sendScoreboardUpdate(w, g);

    ssleep(s, 1);

    pthread_mutex_lock(&a->suspend_time);
    pthread_mutex_unlock(&a->suspend_time);
  }

  pthread_exit(NULL);
}

void *fall_t(void* arg) {
  player_args* a = (player_args*) arg;
  window* w = a->win;
  signaler* in = a->queue;
  signaler* co = a->control;
  signaler* fa = a->fall;
  game_state* g = a->game_state;

  pthread_mutex_lock(&g->mutex);
  update_board(w, g->brd, g->curr);
  pthread_mutex_unlock(&g->mutex);

  while (!is_signaled(fa)) {
    pthread_mutex_lock(&g->mutex);
    int le = g->level;
    pthread_mutex_unlock(&g->mutex);

    msleep(fa, calcFallSpeed(le));

    pthread_mutex_lock(&a->suspend_fall);
    pthread_mutex_unlock(&a->suspend_fall);

    pthread_mutex_lock(&g->mutex);
    if (!g->gameover) {
      int result = moveDown(g->brd, g->curr);
      update_board(w, g->brd, g->curr);
      if (!result) setCurrentPiece(w, g, in, co);
    }
    pthread_mutex_unlock(&g->mutex);
  }

  pthread_exit(NULL);
}

void *player_t(void* arg) {
  player_args* a = (player_args*) arg;
  window* w = a->win;
  signaler* in = a->queue;
  signaler* co = a->control;
  signaler* ti = a->timer;
  signaler* fa = a->fall;
  game_state* g = a->game_state;

  redraw_board(w, g->brd);
  update_summary(w, g->time, g->level, g->score);
  update_preview(w, g->next);

  pthread_mutex_init(&g->mutex, NULL);

  pthread_t t_time;
  pthread_t t_fall;

  pthread_create(&t_time, NULL, timer_t, (void*) a);
  pthread_create(&t_fall, NULL, fall_t, (void*) a);

  while(1) {

    signal_wait(in);

    pthread_mutex_lock(&g->mutex);
    int gameover = g->gameover;
    pthread_mutex_unlock(&g->mutex);

    if (gameover) break;

    pthread_mutex_lock(&a->suspend_game);
    pthread_mutex_unlock(&a->suspend_game);

    int ch = next_result(in);

    pthread_mutex_lock(&g->mutex);

    int result;
    switch(ch) {
      case ACTION_UP:
        setDown(g->brd, g->curr);
        update_board(w, g->brd, g->curr);
        setCurrentPiece(w, g, in, co);
        break;
      case ACTION_DOWN:
        result = moveDown(g->brd, g->curr);
        update_board(w, g->brd, g->curr);
        if (!result) setCurrentPiece(w, g, in, co);
        break;
      case ACTION_LEFT:
        moveLeft(g->brd, g->curr);
        update_board(w, g->brd, g->curr);
        break;
      case ACTION_RIGHT:
        moveRight(g->brd, g->curr);
        update_board(w, g->brd, g->curr);
        break;
      case ACTION_RRIGHT:
        rotateRight(g->brd, g->curr);
        update_board(w, g->brd, g->curr);
        break;
      case ACTION_RLEFT:
        rotateLeft(g->brd, g->curr);
        update_board(w, g->brd, g->curr);
        break;
    }

    pthread_mutex_unlock(&g->mutex);
  }

  clear_preview(w);
  clear_summary(w);
  print_game_over_summary(w, g->time, g->level, g->score, g->lines);

  freeGameState(g);

  send_signal(ti);
  send_signal(fa);
  pthread_join(t_time, NULL);
  pthread_join(t_fall, NULL);

  pthread_exit(NULL);
}

void run_game_controller(signaler* in, window** w, signaler** s, game_state** g, player_args** arg, int n) {
  int paused = 0;
  int end;

  do {
    signal_wait(in);

    int states = 0;
    end = 1;

    for (int i = 0; i < n; i++) {
      pthread_mutex_lock(&g[i]->mutex);
      states |= g[i]->gameover << i;
      end &= g[i]->gameover;
      pthread_mutex_unlock(&g[i]->mutex);
    }

    int ch = next_result(in);
    end |= (ch == ACTION_QUIT && paused);

    if (end) {

      for (int i = 0; i < n; i++) {
        pthread_mutex_lock(&g[i]->mutex);
        int active = !g[i]->gameover;
        g[i]->gameover = 1;

        if (paused && active) {
          redraw_board(w[i], g[i]->brd);
          update_board(w[i], g[i]->brd, g[i]->curr);
        }

        pthread_mutex_unlock(&g[i]->mutex);

        pthread_mutex_unlock(&arg[i]->suspend_time);
        pthread_mutex_unlock(&arg[i]->suspend_fall);
        pthread_mutex_unlock(&arg[i]->suspend_game);

        send_signal(s[i]);
      }

    } else if (ch == ACTION_PAUSE && !paused) {

      for (int i = 0; i < n; i++) {
        if (!(states & (1 << i))) {
          pthread_mutex_lock(&arg[i]->suspend_time);
          pthread_mutex_lock(&arg[i]->suspend_fall);
          pthread_mutex_lock(&arg[i]->suspend_game);

          print_pause(w[i]);
          clear_preview(w[i]);
        }
      }

      paused = 1;

    } else if (ch == ACTION_PAUSE && paused) {

      for (int i = 0; i < n; i++) {
        if (!(states & (1 << i))) {
          pthread_mutex_lock(&g[i]->mutex);
          redraw_board(w[i], g[i]->brd);
          update_board(w[i], g[i]->brd, g[i]->curr);
          update_preview(w[i], g[i]->next);
          pthread_mutex_unlock(&g[i]->mutex);

          pthread_mutex_unlock(&arg[i]->suspend_time);
          pthread_mutex_unlock(&arg[i]->suspend_fall);
          pthread_mutex_unlock(&arg[i]->suspend_game);
        }
      }

      paused = 0;
    }
  } while (!end);
}

void start_game(int n) {
  pthread_t t_keyboard;
  pthread_t t_games[MAX_PLAYERS];
  player_args* arg[MAX_PLAYERS];
  signaler* in = init_signaler();

  kbargs* kb_args = (kbargs*) malloc(sizeof(kbargs));
  kb_args->game_si = in;

  init_tetra(n);

  window* w[MAX_PLAYERS];
  game_state* g[MAX_PLAYERS];
  signaler* s[MAX_PLAYERS];

  for (int i = 0; i < MAX_PLAYERS; i++) { 
    kb_args->player_si[i] = NULL;
    w[i] = NULL;
    g[i] = NULL;
    s[i] = NULL;
  }

  for (int i = 0; i < n; i++) {
    w[i] = init_player_window(i);
    g[i] = initGameState(i);
    s[i] = init_signaler();

    kb_args->player_si[i] = s[i];

    arg[i] = (player_args*) malloc(sizeof(player_args));

    arg[i]->win = w[i];
    arg[i]->queue = s[i];
    arg[i]->game_state = g[i];
    arg[i]->fall = init_signaler();
    arg[i]->timer = init_signaler();
    arg[i]->control = kb_args->game_si;

    pthread_mutex_init(&arg[i]->suspend_time, NULL);
    pthread_mutex_init(&arg[i]->suspend_fall, NULL);
    pthread_mutex_init(&arg[i]->suspend_game, NULL);

    print_game_window(arg[i]->win, i);
    pthread_create(&t_games[i], NULL, player_t, (void*) arg[i]);
  }

  pthread_create(&t_keyboard, NULL, keyboard_t, (void*) kb_args);

  run_game_controller(in, w, s, g, arg, n);

  pthread_cancel(t_keyboard);

  for (int i = 0; i < n; i++) {
    pthread_join(t_games[i], NULL);
  }

  freeTetra();

  while (get_raw_input() != KEY_ESC) continue;

  for (int i = 0; i < n; i++) {
    free_window(arg[i]->win);
    free(arg[i]);
  }
}

#define EXIT -1

int showMenuForResult(window* wm) {
  printMenu(wm);

  while (1) {
    switch(get_raw_input()) {
      case ' ':
      case '1':
        return 1;
      case '2':
        return 2;
      case '3':
        return 3;
      case '4':
        return 4;
      case 'q':
        return EXIT;
    }
  }
}

void startTetris() {
  init_curses();

  window* wm = initMenuWin();
  window* wb = initBoarderWin();
  printBoarder(wb);
  printMenuImage(wm);
  pthread_t t_menuAnim = startMenuAnimateThread(wm);
  get_raw_input();
  pthread_cancel(t_menuAnim);

  while(1) {
    int result = showMenuForResult(wm);

    if (result == EXIT) break;

    clear_menu_window(wm);
    clear_menu_window(wb);

    start_game(result);

    printBoarder(wb);
  }

  free_menu_window(wm);
  free_menu_window(wb);

  close_curses();
}
