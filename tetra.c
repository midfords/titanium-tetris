#include "tetra.h"

#include <pthread.h>
#include <stdlib.h>
#include <time.h>

#include "random.h"
#include "utils.h"

//  []  
//[][][]
//      
const tetra t0 = {
  3,
  {
    0,1,0,
    1,1,1,
    0,0,0
  },
  no, we, so, ea
};

//[][]  
//  [][]
//      
const tetra t1 = {
  3,
  {
    2,2,0,
    0,2,2,
    0,0,0
  },
  no, we, no, we
};

//  [][]
//[][]  
//      
const tetra t2 = {
  3,
  {
    0,3,3,
    3,3,0,
    0,0,0
  },
  no, we, no, we
};

//[][]
//[][]
const tetra t3 = {
  2,
  {
    4,4,
    4,4
  },
  no, no, no, no
};

//  []    
//  []    
//  []    
//  []    
const tetra t4 = {
  4,
  {
    0,5,0,0,
    0,5,0,0,
    0,5,0,0,
    0,5,0,0
  },
  no, we, no, we
};

//  []  
//  []  
//  [][]
const tetra t5 = {
  3,
  {
    0,6,0,
    0,6,0,
    0,6,6
  },
  no, we, so, ea
};

//  []  
//  []  
//[][]  
const tetra t6 = {
  3,
  {
    0,7,0,
    0,7,0,
    7,7,0
  },
  no, we, so, ea
};

pthread_mutex_t tetra_mutex;
int players;
unsigned long **player_seeds;

tetra getTetra(int i) {
  switch (i) {
    case 0:
      return t0;
    case 1:
      return t1;
    case 2:
      return t2;
    case 3:
      return t3;
    case 4:
      return t4;
    case 5:
      return t5;
    default:
      return t6;
  }
}

tetra requestTetra(int p) {
  pthread_mutex_lock(&tetra_mutex);

  int t = (int)(getDouble(player_seeds[p]) * 7.0);

  pthread_mutex_unlock(&tetra_mutex);

  return getTetra(t);
}

void init_tetra(int n) {
  unsigned long seed = (unsigned long)time(NULL);

  players = n;
  player_seeds = (unsigned long **)malloc(sizeof(unsigned long *) * n);

  for (int i = 0; i < n; i++) {
    player_seeds[i] = (unsigned long *)malloc(sizeof(unsigned long));
    *player_seeds[i] = seed;
    getDouble(player_seeds[i]);
  }

  pthread_mutex_init(&tetra_mutex, NULL);
}

void freeTetra() {
  pthread_mutex_lock(&tetra_mutex);

  int i;
  for (i = 0; i < players; i++) free(player_seeds[i]);

  free(player_seeds);

  pthread_mutex_unlock(&tetra_mutex);
}
