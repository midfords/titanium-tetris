
#include "game.h"

#include <stdlib.h>

#include "tetra.h"
#include "utils.h"

#define LEVEL_UP 7500
#define MIN_MS 500
#define MAX_MS 60
#define MAX_LVL 10

#define PIECE 50
#define SINGLE 200
#define DOUBLE 500
#define TRIPLE 1000
#define TETRIS 2000

int validate(int* br, tetra_state ns) {
  tetra t = ns.t;

  int valid = 1;
  int i, j;
  for (i = 0; i < t.l && valid; i++) {
    for (j = 0; j < t.l && valid; j++) {
      int ry = ns.y + i;
      int rx = ns.x + j;

      valid = valid && ( t.shape[ns.dir_ptr(i, j, t.l)] == 0 || 
        ( ( ry < BOARD_Y ) && ( ry >= 0 ) && 
          ( rx < BOARD_X ) && ( rx >= 0 ) && 
          br[p(ry, rx, BOARD_Y, BOARD_X)] == 0 ) );
    }
  }

  return valid;
}

int tryMove(int* br, tetra_state* ts, int nx, int ny, int (*np)(int, int, int)) {
  tetra_state ns = {
    ts->t,
    np,
    nx,
    ny
  };

  if (validate(br, ns)) {
    ts->x = nx;
    ts->y = ny;
    ts->dir_ptr = np;
    return 1;
  }

  return 0;
}

int moveLeft(int* br, tetra_state* ts) {
  return tryMove(br, ts, ts->x - 1, ts->y, ts->dir_ptr);
}

int moveRight(int* br, tetra_state* ts) {
  return tryMove(br, ts, ts->x + 1, ts->y, ts->dir_ptr);
}

int moveDown(int* br, tetra_state* ts) {
  return tryMove(br, ts, ts->x, ts->y + 1, ts->dir_ptr);
}

int setDown(int* br, tetra_state* ts) {
  while (moveDown(br, ts)) {}
  return 1;
}

int tryRotate(int* br, tetra_state* ts, int (*nptr)(int, int, int)) {
  return tryMove(br, ts, ts->x, ts->y, nptr) ||
         tryMove(br, ts, ts->x - 1, ts->y, nptr) ||
         tryMove(br, ts, ts->x + 1, ts->y, nptr) ||
         tryMove(br, ts, ts->x, ts->y - 1, nptr) ||
         tryMove(br, ts, ts->x - 1, ts->y + 1, nptr) ||
         tryMove(br, ts, ts->x + 1, ts->y + 1, nptr) ||
         tryMove(br, ts, ts->x - 2, ts->y, nptr) ||
         tryMove(br, ts, ts->x + 2, ts->y, nptr) ||
         tryMove(br, ts, ts->x, ts->y - 2, nptr);
}

int rotateLeft(int* br, tetra_state* ts) {
  int (*nptr)(int,int,int);

  if (ts->dir_ptr == no)
    nptr = ts->t.we_func;
  else if (ts->dir_ptr == we)
    nptr = ts->t.so_func;
  else if (ts->dir_ptr == so)
    nptr = ts->t.ea_func;
  else
    nptr = ts->t.no_func;

  return tryRotate(br, ts, nptr);
}

int rotateRight(int* br, tetra_state* ts) {
  int (*nptr)(int,int,int);

  if (ts->dir_ptr == no)
    nptr = ts->t.ea_func;
  else if (ts->dir_ptr == ea)
    nptr = ts->t.so_func;
  else if (ts->dir_ptr == so)
    nptr = ts->t.we_func;
  else
    nptr = ts->t.no_func;

  return tryRotate(br, ts, nptr);
}

int calcScore(int li) {
  switch(li) {
    case 1:
      return SINGLE;
    case 2:
      return DOUBLE;
    case 3:
      return TRIPLE;
    case 4:
      return TETRIS;
    default:
      return PIECE;
  }
}

int calcLevel(int sc) {
  return (sc / LEVEL_UP) + 1;
}

int calcFallSpeed(int le) {
  return (int) ( MIN_MS - (MIN_MS - MAX_MS) / (MAX_LVL - 1) 
      * ((le > MAX_LVL ? MAX_LVL : le) - 1) );
}

tetra_state* getNewTetraState(tetra t) {
  tetra_state* s = (tetra_state*) malloc(sizeof(tetra_state));
  s->t = t;
  s->dir_ptr = no;
  s->y = 0;
  s->x = BOARD_X / 2 - 2;

  return s;
}

game_state* initGameState(int p) {
  tetra t;

  game_state* g = (game_state*) malloc(sizeof(game_state));
  g->brd = (int*) calloc(BOARD_X * BOARD_Y, sizeof(int));
  t = requestTetra(p);
  g->curr = getNewTetraState(t);
  t = requestTetra(p);
  g->next = getNewTetraState(t);

  g->player = p;
  g->gameover = 0;
  g->time = 0;
  g->level = 1;
  g->score = 0;
  g->lines = 0;

  return g;
}

void freeGameState(game_state* g) {
  free(g->brd);
  free(g->curr);
  free(g->next);
  free(g);
}
