
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include "print.h"
#include "tetris.h"

void sigIntHandler() {
  close_curses();

  printf("Program was interrupted.\r\n");

  exit(0);
}

int main() {
  signal(SIGINT, sigIntHandler);

  startTetris();

  printf("Program exited successfully.\r\n");

  return 0;
}
