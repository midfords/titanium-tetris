
#ifndef GAME_H_
#define GAME_H_

#include <pthread.h>

#include "tetra.h"

#define BOARD_X 10
#define BOARD_Y 20

typedef struct tetra_state {
  tetra t;

  int (*dir_ptr)(int, int, int);

  int x;
  int y;
} tetra_state;

typedef struct game_state {
  pthread_mutex_t mutex;

  int* brd;

  int player;
  int gameover;
  int time;
  int level;
  int score;
  int lines;

  tetra_state* curr;
  tetra_state* next;
} game_state;

tetra_state* getNewTetraState(tetra t);

game_state* initGameState(int p);

void freeGameState(game_state* g);

int moveLeft(int* br, tetra_state* ts);

int moveRight(int* br, tetra_state* ts);

int moveDown(int* br, tetra_state* ts);

int setDown(int* br, tetra_state* ts);

int rotateLeft(int* br, tetra_state* ts);

int rotateRight(int* br, tetra_state* ts);

int calcScore(int li);

int calcLevel(int sc);

int calcFallSpeed(int le);

#endif
