
CC = gcc
CFLAGS = -Wall -g -pthread

PROG = ./tetris

HDRS = input.h tetris.h tetra.h print.h menu.h game.h signaler.h utils.h random.h
SRCS = main.c input.c tetris.c tetra.c print.c menu.c game.c signaler.c utils.c random.c
OBJS = main.o input.o tetris.o tetra.o print.o menu.o game.o signaler.o utils.o random.o

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(PROG) -lncurses 

main.o: main.c $(HDRS)
	$(CC) $(CFLAGS) -c main.c -o main.o

input.o: input.c $(HDRS)
	$(CC) $(CFLAGS) -c input.c -o input.o

tetris.o: tetris.c $(HDRS)
	$(CC) $(CFLAGS) -c tetris.c -o tetris.o

tetra.o: tetra.c $(HDRS)
	$(CC) $(CFLAGS) -c tetra.c -o tetra.o

print.o: print.c $(HDRS)
	$(CC) $(CFLAGS) -c print.c -o print.o

menu.o: menu.c $(HDRS)
	$(CC) $(CFLAGS) -c menu.c -o menu.o

game.o: game.c $(HDRS)
	$(CC) $(CFLAGS) -c game.c -o game.o

utils.o: utils.c $(HDRS)
	$(CC) $(CFLAGS) -c utils.c -o utils.o

signaler.o: signaler.c $(HDRS)
	$(CC) $(CFLAGS) -c signaler.c -o signaler.o

random.o: random.c $(HDRS)
	$(CC) $(CFLAGS) -c random.c -o random.o

clean:
	-rm $(OBJS) $(PROG)
