
#include "print.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "game.h"
#include "tetra.h"
#include "utils.h"
#include "signaler.h"

#define END_Y 24
#define END_X 46

#define BLOCK "  "
#define SHADOW 8

#define ANIMATION_MS 15
#define TETRIS_C "TETRIS"

#define WINDOW_OFF 41
#define BOARD_OFF_X 9
#define BOARD_OFF_Y 3
#define PREVIEW_X 32
#define PREVIEW_Y 4
#define SUMMARY_X 32
#define SUMMARY_Y 14
#define TIME_X 33
#define TIME_Y 15
#define LEVEL_X 33
#define LEVEL_Y 17
#define SCORE_X 33
#define SCORE_Y 20
#define GAME_OV_X 12
#define GAME_OV_Y 7

pthread_mutex_t print_mutex;
tetra_state* shadow;

void init_curses() {
  initscr();
  noecho();
  keypad(stdscr, TRUE);

  start_color();
  init_pair(0, COLOR_WHITE, COLOR_BLACK);
  init_pair(1, COLOR_BLACK, COLOR_RED);
  init_pair(2, COLOR_BLACK, COLOR_GREEN);
  init_pair(3, COLOR_BLACK, COLOR_YELLOW);
  init_pair(4, COLOR_BLACK, COLOR_BLUE);
  init_pair(5, COLOR_BLACK, COLOR_MAGENTA);
  init_pair(6, COLOR_BLACK, COLOR_CYAN);
  init_pair(7, COLOR_BLACK, COLOR_WHITE);
  init_pair(8, COLOR_BLACK, COLOR_GREY);

  init_pair(11, COLOR_RED, COLOR_BLACK);
  init_pair(12, COLOR_GREEN, COLOR_BLACK);
  init_pair(13, COLOR_YELLOW, COLOR_BLACK);
  init_pair(14, COLOR_BLUE, COLOR_BLACK);
  init_pair(15, COLOR_MAGENTA, COLOR_BLACK);
  init_pair(16, COLOR_CYAN, COLOR_BLACK);
  init_pair(17, COLOR_WHITE, COLOR_BLACK);

  clear();
  refresh();

  pthread_mutex_init(&print_mutex, NULL);
  shadow = (tetra_state*)malloc(sizeof(tetra_state));
}

window* init_player_window(int n) {
  window* w = (window*)malloc(sizeof(window));
  w->id = n;
  w->win = newwin(25, 47, 0, WINDOW_OFF * n);
  w->state = calloc(BOARD_Y * BOARD_X, sizeof(int));
  return w;
}

void free_menu_window(window* w) {
  wclear(w->win);
  wrefresh(w->win);
  delwin(w->win);
}

void free_window(window* w) {
  free(w->state);
  wclear(w->win);
  wrefresh(w->win);
  delwin(w->win);
  free(w);
}

void clear_menu_window(window* w) {
  wclear(w->win);
  wrefresh(w->win);
}

void close_curses() {
  clear();
  refresh();
  free(shadow);
  reset_shell_mode();
}

char* format_time(int t) {
  char* time = (char*) malloc(sizeof(8));

  int h = t / 3600;
  int m = (t - h * 3600) / 60;
  int s = (t - m * 60);

  sprintf(time, "%01d:%02d:%02d", h, m, s);

  return time;
}

void apply(window* w) {
  wmove(w->win, END_Y, END_X);
  wrefresh(w->win);
}

void clear_board(window* w) {
  int i;
  for (i = BOARD_OFF_Y; i < BOARD_Y + BOARD_OFF_Y; i++) {
    mvwprintw(w->win, i, BOARD_OFF_X, "                    ");
  }
  apply(w);
}

void clear_summary(window* w) {
  pthread_mutex_lock(&print_mutex);

  int i;
  for (i = SUMMARY_Y; i < SUMMARY_Y + 7; i++) {
    mvwprintw(w->win, i, SUMMARY_X, "         ");
  }
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void clear_preview(window* w) {
  pthread_mutex_lock(&print_mutex);

  int i;
  for (i = PREVIEW_Y; i < PREVIEW_Y + 5; i++) {
    mvwprintw(w->win, i, PREVIEW_X, "         ");
  }
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void clear_block(window* w, int y, int x) {
  wattron(w->win, COLOR_PAIR(0));
  mvwprintw(w->win, y, x, BLOCK);
  wattroff(w->win, COLOR_PAIR(0));
}

void clear_board_block(window* w, int y, int x) {
  clear_block(w, y + BOARD_OFF_Y, x * 2 + BOARD_OFF_X);
  apply(w);
}

void print_game_1_window(window* w) {
  pthread_mutex_lock(&print_mutex);

  mvwprintw(w->win, 0,  0, " __________________________________________ ");
  mvwprintw(w->win, 1,  0, "| |   |_  |_   _|______|  _|___| |   |___  |");
  mvwprintw(w->win, 2,  0, "|_|___| |___|_|______|___|___  | |___| | |_|");
  mvwprintw(w->win, 3,  0, "|_|_____|                    |_|___|___|_  |");
  mvwprintw(w->win, 4,  0, "| | |_  |                    | |         |_|");
  mvwprintw(w->win, 5,  0, "| |_  | |                    | |         | |");
  mvwprintw(w->win, 6,  0, "| |_|_|_|                    | |         | |");
  mvwprintw(w->win, 7,  0, "|_|___  |                    |_|         |_|");
  mvwprintw(w->win, 8,  0, "|   | |_|                    | |         | |");
  mvwprintw(w->win, 9,  0, "|___| | |                    |_|---------| |");
  mvwprintw(w->win, 10, 0, "|_| | |_|                    | |_  | |_|___|");
  mvwprintw(w->win, 11, 0, "|  _|_| |                    |___| |  _|_  |");
  mvwprintw(w->win, 12, 0, "|_|_____|                    | | |_|_|   | |");
  mvwprintw(w->win, 13, 0, "| |_   _|                    | |_____|___|_|");
  mvwprintw(w->win, 14, 0, "|_  |_| |                    |_|         | |");
  mvwprintw(w->win, 15, 0, "| |_|___|                    | |         | |");
  mvwprintw(w->win, 16, 0, "|_____| |                    | |         |_|");
  mvwprintw(w->win, 17, 0, "|  _|  _|                    | |         | |");
  mvwprintw(w->win, 18, 0, "| | |_| |                    |_|         | |");
  mvwprintw(w->win, 19, 0, "|_|  _| |                    |_|         | |");
  mvwprintw(w->win, 20, 0, "| |_| | |                    | |         |_|");
  mvwprintw(w->win, 21, 0, "| |_| |_|                    | |---------| |");
  mvwprintw(w->win, 22, 0, "| |___| |                    |___|_   _|___|");
  mvwprintw(w->win, 23, 0, "|_|_____|--------------------| |   |_|  _| |");
  mvwprintw(w->win, 24, 0, "|___|_____|___|___|_______|____|___|_ _|___|");
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void print_game_extend_window(window* w) {
  pthread_mutex_lock(&print_mutex);

  mvwprintw(w->win, 0,  0, "___________________________________________ ");
  mvwprintw(w->win, 1,  0, "____| |_  |_   _|______|  _|___| |   |___  |");
  mvwprintw(w->win, 2,  0, "  |  _| |___|_|______|___|___  | |___| | |_|");
  mvwprintw(w->win, 3,  0, "__|_|__ |                    |_|___|___|_  |");
  mvwprintw(w->win, 4,  0, "| | | |_|                    | |         |_|");
  mvwprintw(w->win, 5,  0, "| | |_  |                    | |         | |");
  mvwprintw(w->win, 6,  0, "| |_ _|_|                    | |         | |");
  mvwprintw(w->win, 7,  0, "|_|___  |                    |_|         |_|");
  mvwprintw(w->win, 8,  0, "|   | |_|                    | |         | |");
  mvwprintw(w->win, 9,  0, "|___|_  |                    |_|---------| |");
  mvwprintw(w->win, 10, 0, "  |   |_|                    | |_  | |_|___|");
  mvwprintw(w->win, 11, 0, "| |___| |                    |___| |  _|_  |");
  mvwprintw(w->win, 12, 0, "|_|_____|                    | | |_|_|   | |");
  mvwprintw(w->win, 13, 0, "| |_   _|                    | |_____|___|_|");
  mvwprintw(w->win, 14, 0, "|_  |_| |                    |_|         | |");
  mvwprintw(w->win, 15, 0, "| |_|___|                    | |         | |");
  mvwprintw(w->win, 16, 0, "|_____| |                    | |         |_|");
  mvwprintw(w->win, 17, 0, "|  _|  _|                    | |         | |");
  mvwprintw(w->win, 18, 0, "| | |_| |                    |_|         | |");
  mvwprintw(w->win, 19, 0, "|_|  _| |                    |_|         | |");
  mvwprintw(w->win, 20, 0, "| |_| | |                    | |         |_|");
  mvwprintw(w->win, 21, 0, "|  _| |_|                    | |---------| |");
  mvwprintw(w->win, 22, 0, "|_|___| |                    |___|_   _|  _|");
  mvwprintw(w->win, 23, 0, "|  _|  _|--------------------| |   |_| |_| |");
  mvwprintw(w->win, 24, 0, "__|___|___|___|___|_______|____|___|_|_|___|");
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void print_game_window(window* w, int p) {
  if (p < 1) {
    print_game_1_window(w);
  } else {
    print_game_extend_window(w);
  }
}

void print_player_1_startup(window* w) {
  pthread_mutex_lock(&print_mutex);

  mvwprintw(w->win, 3 + BOARD_OFF_Y, BOARD_OFF_X, "      Player 1      ");
  mvwprintw(w->win, 7 + BOARD_OFF_Y, BOARD_OFF_X, "     Get Ready!     ");
  mvwprintw(w->win, 11 + BOARD_OFF_Y, BOARD_OFF_X, "          ▲         ");
  mvwprintw(w->win, 12 + BOARD_OFF_Y, BOARD_OFF_X, "        ◄ ▼ ►       ");
  mvwprintw(w->win, 14 + BOARD_OFF_Y, BOARD_OFF_X, "      <space>  ⟳    ");
  mvwprintw(w->win, 15 + BOARD_OFF_Y, BOARD_OFF_X, "        <.>    ⟲    ");
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void print_player_2_startup(window* w) {
  pthread_mutex_lock(&print_mutex);

  mvwprintw(w->win, 3 + BOARD_OFF_Y, BOARD_OFF_X, "      Player 2      ");
  mvwprintw(w->win, 7 + BOARD_OFF_Y, BOARD_OFF_X, "     Get Ready!     ");
  mvwprintw(w->win, 11 + BOARD_OFF_Y, BOARD_OFF_X, "          w         ");
  mvwprintw(w->win, 12 + BOARD_OFF_Y, BOARD_OFF_X, "        a s d       ");
  mvwprintw(w->win, 14 + BOARD_OFF_Y, BOARD_OFF_X, "         z   ⟳      ");
  mvwprintw(w->win, 15 + BOARD_OFF_Y, BOARD_OFF_X, "         x   ⟲      ");
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void print_player_3_startup(window* w) {
  pthread_mutex_lock(&print_mutex);

  mvwprintw(w->win, 3 + BOARD_OFF_Y, BOARD_OFF_X, "      Player 3      ");
  mvwprintw(w->win, 7 + BOARD_OFF_Y, BOARD_OFF_X, "     Get Ready!     ");
  mvwprintw(w->win, 11 + BOARD_OFF_Y, BOARD_OFF_X, "          t         ");
  mvwprintw(w->win, 12 + BOARD_OFF_Y, BOARD_OFF_X, "        f g h       ");
  mvwprintw(w->win, 14 + BOARD_OFF_Y, BOARD_OFF_X, "         v   ⟳      ");
  mvwprintw(w->win, 15 + BOARD_OFF_Y, BOARD_OFF_X, "         b   ⟲      ");
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void print_player_4_startup(window* w) {
  pthread_mutex_lock(&print_mutex);

  mvwprintw(w->win, 3 + BOARD_OFF_Y, BOARD_OFF_X, "      Player 4      ");
  mvwprintw(w->win, 7 + BOARD_OFF_Y, BOARD_OFF_X, "     Get Ready!     ");
  mvwprintw(w->win, 11 + BOARD_OFF_Y, BOARD_OFF_X, "          i         ");
  mvwprintw(w->win, 12 + BOARD_OFF_Y, BOARD_OFF_X, "        j k l       ");
  mvwprintw(w->win, 14 + BOARD_OFF_Y, BOARD_OFF_X, "         m     ⟳    ");
  mvwprintw(w->win, 15 + BOARD_OFF_Y, BOARD_OFF_X, "        <,>    ⟲    ");
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void print_pause(window* w) {
  pthread_mutex_lock(&print_mutex);

  clear_board(w);
  mvwprintw(w->win, 6 + BOARD_OFF_Y, BOARD_OFF_X,  "       Paused       ");
  mvwprintw(w->win, 10 + BOARD_OFF_Y, BOARD_OFF_X, "     [P] Unpause    ");
  mvwprintw(w->win, 12 + BOARD_OFF_Y, BOARD_OFF_X, "     [Q] Quit      ");
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void print_game_over_summary(window* w, int t, int l, int s, int li) {
  pthread_mutex_lock(&print_mutex);

  char* time = format_time(t);

  mvwprintw(w->win, 0 + GAME_OV_Y, GAME_OV_X, "+------------+");
  mvwprintw(w->win, 1 + GAME_OV_Y, GAME_OV_X, "| Game Over. |");
  mvwprintw(w->win, 2 + GAME_OV_Y, GAME_OV_X, "+------------+");
  mvwprintw(w->win, 3 + GAME_OV_Y, GAME_OV_X, "| Time:      |");
  mvwprintw(w->win, 4 + GAME_OV_Y, GAME_OV_X, "|    %s |", time);
  mvwprintw(w->win, 5 + GAME_OV_Y, GAME_OV_X, "| Level:     |");
  mvwprintw(w->win, 6 + GAME_OV_Y, GAME_OV_X, "|         %2d |", l);
  mvwprintw(w->win, 7 + GAME_OV_Y, GAME_OV_X, "| Score:     |");
  mvwprintw(w->win, 8 + GAME_OV_Y, GAME_OV_X, "|    %7d |", s);
  mvwprintw(w->win, 9 + GAME_OV_Y, GAME_OV_X, "| Lines:     |");
  mvwprintw(w->win, 10 + GAME_OV_Y, GAME_OV_X, "|       %4d |", li);
  mvwprintw(w->win, 11 + GAME_OV_Y, GAME_OV_X, "+------------+");
  apply(w);

  free(time);
  pthread_mutex_unlock(&print_mutex);
}

void print_block(window* w, int y, int x, int color) {
  wattron(w->win, COLOR_PAIR(color));
  mvwprintw(w->win, y, x, BLOCK);
  wattroff(w->win, COLOR_PAIR(color));
}

void print_board_block(window* w, int y, int x, int color) {
  print_block(w, y + BOARD_OFF_Y, x * 2 + BOARD_OFF_X, color);
  apply(w);
}

void printTetra(window* w, tetra_state* s) {
  tetra t = s->t;
  int i, j;
  for (i = 0; i < t.l; i++) {
    for (j = 0; j < t.l; j++) {
      if (t.shape[s->dir_ptr(i, j, t.l)] > 0) {
        print_block(w, i + s->y + BOARD_OFF_Y, (j + s->x) * 2 + BOARD_OFF_X, t.shape[s->dir_ptr(i, j, t.l)]);
      }
    }
  }
  apply(w);
}

void print_shadow(window* w, tetra_state* s) {
  tetra t = s->t;
  int i, j;
  for (i = 0; i < t.l; i++) {
    for (j = 0; j < t.l; j++) {
      if (t.shape[s->dir_ptr(i, j, t.l)] > 0) {
        print_block(w, i + s->y + BOARD_OFF_Y, (j + s->x) * 2 + BOARD_OFF_X, SHADOW);
      }
    }
  }
  apply(w);
}

void print_preview(window* w, tetra_state* s) {
  pthread_mutex_lock(&print_mutex);

  tetra t = s->t;
  int i, j;
  for (i = 0; i < t.l; i++) {
    for (j = 0; j < t.l; j++) {
      if (t.shape[s->dir_ptr(i, j, t.l)] > 0) {
        print_block(w, i + PREVIEW_Y + 1, j * 2 + PREVIEW_X + 2, t.shape[s->dir_ptr(i, j, t.l)]);
      }
    }
  }
  apply(w);

  pthread_mutex_unlock(&print_mutex);
}

void print_char(window* w, int y, int x, int ch) {
  mvwprintw(w->win, y + BOARD_OFF_Y, x * 2 + BOARD_OFF_X, "%c", ch);
  apply(w);
}

typedef struct animate_args {
  window* win;
  int line;
  int tetris;
} anim_args;

void* animate_clear_line_t(void* arg) {
  anim_args* args = (anim_args*) arg;
  window* w = args->win;
  int line = args->line;
  int tetris = args->tetris;

  int i;
  for(i = 0; i < BOARD_X; i++) {
    pthread_mutex_lock(&print_mutex);

    clear_board_block(w, line, i);

    if(tetris && i > 2 && i < 9)
      print_char(w, line, i, TETRIS_C[i - 3]);

    pthread_mutex_unlock(&print_mutex);
    msleep(NULL, ANIMATION_MS);
  }

  if (tetris)
    msleep(NULL, ANIMATION_MS * 3);

  free(args);
  pthread_exit(NULL);
}

pthread_t start_clear_line_animation(window* w, int l, int t) {
  pthread_t line_t;
  anim_args* args = (anim_args*) malloc(sizeof(anim_args));
  args->win = w;
  args->line = l;
  args->tetris = t;

  pthread_create(&line_t, NULL, animate_clear_line_t, (void*) args);

  return line_t;
}

void redraw_board(window* w, int* brd) {
  pthread_mutex_lock(&print_mutex);

  int i, j;
  for (i = 0; i < BOARD_Y; i++) {
    for (j = 0; j < BOARD_X; j++) {
      int ij = p(i, j, BOARD_Y, BOARD_X);
      w->state[ij] = brd[ij];
      print_block(w, i + BOARD_OFF_Y, j * 2 + BOARD_OFF_X, brd[ij]);
    }
  }

  pthread_mutex_unlock(&print_mutex);
}

void update_board(window* w, int* brd, tetra_state* ts) {
  pthread_mutex_lock(&print_mutex);

  int* state = w->state;

  int i, j;
  for(i = 0; i < BOARD_Y; i++) {
    for(j = 0; j < BOARD_X; j++) {
      int ij = p(i, j, BOARD_Y, BOARD_X);

      if (state[ij] != brd[ij]) {
        state[ij] = brd[ij];
        print_board_block(w, i, j, brd[ij]);
      }
    }
  }

  processTetraShadow(brd, ts, shadow);
  writeTetraShadowToBoard(state, shadow);
  print_shadow(w, shadow);

  writeTetraToBoard(state, ts);
  printTetra(w, ts);

  pthread_mutex_unlock(&print_mutex);
}

void update_preview(window* w, tetra_state* ts) {
  clear_preview(w);
  print_preview(w, ts);
}

void update_time(window* w, int t) {
  char* time = format_time(t);

  mvwprintw(w->win, TIME_Y, TIME_X, "%s", time);
  apply(w);

  free(time);
}

void update_level(window* w, int l) {
  mvwprintw(w->win, LEVEL_Y, LEVEL_X, "Lvl  %2d", l);
  apply(w);
}

void update_score(window* w, int s) {
  mvwprintw(w->win, SCORE_Y, SCORE_X, "%7d", s);
  apply(w);
}

void update_summary(window* w, int t, int l, int s) {
  pthread_mutex_lock(&print_mutex);

  update_time(w, t);
  update_level(w, l);
  update_score(w, s);

  pthread_mutex_unlock(&print_mutex);
}

