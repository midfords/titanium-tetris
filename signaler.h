
#ifndef SIGNALER_H_
#define SIGNALER_H_

#include <pthread.h>

typedef struct signaler {
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  int signaled;
  int result;
} signaler;

signaler* init_signaler();
void free_signaler(signaler* s);

void send_signal(signaler* s);
void send_signal_result(signaler* s, int result);

int is_signaled(signaler* s);
int next_result(signaler* s);

void signal_wait(signaler* s);
void msleep(signaler* s, int ms);
void ssleep(signaler* s, int sec);

#endif
