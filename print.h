
#ifndef PRINT_H_
#define PRINT_H_

#include <curses.h>
#include <pthread.h>

#include "game.h"

#define COLOR_GREY 8

typedef struct window {
  int id;
  int* state;
  WINDOW* win;
} window;

void init_curses();

window* init_menu_window();

window* init_player_window(int n);

void free_menu_window(window* w);

void free_window(window* w);

void close_curses();

void clear_menu_window(window* w);

void clear_summary(window* w);

void clear_preview(window* w);

void print_game_window(window* w, int p);

void print_menu(window* w);

void print_pause(window* w);

void print_game_over_summary(window* w, int t, int l, int s, int li);

pthread_t start_clear_line_animation(window* w, int l, int t);

void redraw_board(window* w, int* brd);

void update_board(window* w, int* brd, tetra_state* ts);

void update_preview(window* w, tetra_state* ts);

void update_summary(window* w, int t, int l, int s);

#endif
