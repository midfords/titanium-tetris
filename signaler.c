
#include "signaler.h"

#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define NO_RESULT -1

signaler* init_signaler() {
  signaler* s = (signaler*)malloc(sizeof(signaler));
  pthread_mutex_init(&s->mutex, NULL);
  pthread_cond_init(&s->cond, NULL);
  s->signaled = false;
  s->result = NO_RESULT;

  return s;
}

void free_signaler(signaler* s) { free(s); }

void send_signal(signaler* s) {
  pthread_mutex_lock(&s->mutex);
  s->signaled = true;
  pthread_cond_signal(&s->cond);
  pthread_mutex_unlock(&s->mutex);
}

void send_signal_result(signaler* s, int result) {
  pthread_mutex_lock(&s->mutex);
  s->result = result;
  s->signaled = true;
  pthread_cond_signal(&s->cond);
  pthread_mutex_unlock(&s->mutex);
}

int is_signaled(signaler* s) {
  pthread_mutex_lock(&s->mutex);
  int signaled = s->signaled;
  pthread_mutex_unlock(&s->mutex);
  return signaled;
}

int next_result(signaler* s) {
  pthread_mutex_lock(&s->mutex);
  int next = s->result;
  s->result = NO_RESULT;
  pthread_mutex_unlock(&s->mutex);
  return next;
}

void signal_wait(signaler* s) {
  pthread_mutex_lock(&s->mutex);
  while (!s->signaled) pthread_cond_wait(&s->cond, &s->mutex);
  s->signaled = false;
  pthread_mutex_unlock(&s->mutex);
}

void sleep(signaler* s, struct timespec ts, int ms) {
  if (s == NULL) {
    nanosleep(&ts, NULL);
    return;
  }

  clock_gettime(CLOCK_REALTIME, &ts);
  ts.tv_nsec += (ms % 1000) * 1000000;
  ts.tv_sec += ms / 1000;
  ts.tv_sec += ts.tv_nsec / 1000000000;
  ts.tv_nsec %= 1000000000;

  pthread_mutex_lock(&s->mutex);
  pthread_cond_timedwait(&s->cond, &s->mutex, &ts);
  pthread_mutex_unlock(&s->mutex);
}

void msleep(signaler* s, int ms) {
  struct timespec ts;

  ts.tv_sec = ms / 1000;
  ts.tv_nsec = (ms % 1000) * 1000000;

  sleep(s, ts, ms);
}

void ssleep(signaler* s, int sec) { msleep(s, sec * 1000); }
