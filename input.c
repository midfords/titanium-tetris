
#include "input.h"

#include <ctype.h>
#include <curses.h>
#include <stdlib.h>

#include "utils.h"

int get_raw_input() { return tolower(getch()); }

void set_next(signaler* si, int action) {
  if (si == NULL) return;

  send_signal_result(si, action);
}

void* keyboard_t(void* arg) {
  kbargs* args = (kbargs*)arg;
  signaler** pc = args->player_si;
  signaler* gc = args->game_si;

  int ch;
  while ((ch = get_raw_input())) {
    switch (ch) {
      case KEY_UP:
        set_next(pc[0], ACTION_UP);
        break;
      case KEY_LEFT:
        set_next(pc[0], ACTION_LEFT);
        break;
      case KEY_DOWN:
        set_next(pc[0], ACTION_DOWN);
        break;
      case KEY_RIGHT:
        set_next(pc[0], ACTION_RIGHT);
        break;
      case ' ':
        set_next(pc[0], ACTION_RRIGHT);
        break;
      case '.':
        set_next(pc[0], ACTION_RLEFT);
        break;
      case 'w':
        set_next(pc[1], ACTION_UP);
        break;
      case 'a':
        set_next(pc[1], ACTION_LEFT);
        break;
      case 's':
        set_next(pc[1], ACTION_DOWN);
        break;
      case 'd':
        set_next(pc[1], ACTION_RIGHT);
        break;
      case 'z':
        set_next(pc[1], ACTION_RRIGHT);
        break;
      case 'x':
        set_next(pc[1], ACTION_RLEFT);
        break;
      case 't':
        set_next(pc[2], ACTION_UP);
        break;
      case 'f':
        set_next(pc[2], ACTION_LEFT);
        break;
      case 'g':
        set_next(pc[2], ACTION_DOWN);
        break;
      case 'h':
        set_next(pc[2], ACTION_RIGHT);
        break;
      case 'v':
        set_next(pc[2], ACTION_RRIGHT);
        break;
      case 'b':
        set_next(pc[2], ACTION_RLEFT);
        break;
      case 'i':
        set_next(pc[3], ACTION_UP);
        break;
      case 'j':
        set_next(pc[3], ACTION_LEFT);
        break;
      case 'k':
        set_next(pc[3], ACTION_DOWN);
        break;
      case 'l':
        set_next(pc[3], ACTION_RIGHT);
        break;
      case 'm':
        set_next(pc[3], ACTION_RRIGHT);
        break;
      case ',':
        set_next(pc[3], ACTION_RLEFT);
        break;
      case 'p':
        set_next(gc, ACTION_PAUSE);
        break;
      case 'q':
        set_next(gc, ACTION_QUIT);
        break;
    }
  }

  pthread_exit(NULL);
}
